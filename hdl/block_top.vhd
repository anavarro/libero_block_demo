library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity block_top is
  port (
    clk        : in  std_logic;
    valid_arr  : in  std_logic_vector(255 downto 0);
    index      : out unsigned(7 downto 0) := (others => '0')
  );
end entity;

architecture behavioral of block_top is
  signal valid_arr_s : std_logic_vector(valid_arr'range) := (others => '0');
  signal index_s     : unsigned(index'range) := (others => '0');
  
begin

  -- register input and output to ensure timing verification
  valid_arr_s <= valid_arr when rising_edge(clk);
  index <= index_s;


  -- use signal (_s) versions of the ports in this process!!
  process(clk)
  begin
    if rising_edge(clk) then
      for i in valid_arr_s'range loop
        if valid_arr_s(i) = '1' then
          index_s <= to_unsigned(i,8);
        end if;
      end loop;
    end if;
  end process;

end architecture;
