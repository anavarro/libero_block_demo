quietly set ACTELLIBNAME PolarFire
quietly set PROJECT_DIR "C:/libero_block_demo"

if {[file exists presynth/_info]} {
   echo "INFO: Simulation library presynth already exists"
} else {
   file delete -force presynth 
   vlib presynth
}
vmap presynth presynth
vmap PolarFire "C:/Microsemi/Libero_SoC_v2022.1/Designer/lib/modelsimpro/precompiled/vlog/PolarFire"

vcom -2008 -explicit  -work presynth "${PROJECT_DIR}/hdl/block_top.vhd"
vcom -2008 -explicit  -work presynth "${PROJECT_DIR}/stimulus/block_tb.vhd"

vsim -L PolarFire -L presynth  -t 1ps -pli C:/Microsemi/Libero_SoC_v2022.1/Designer/lib/modelsimpro/pli/pf_crypto_win_me_pli.dll presynth.block_tb
add wave /block_tb/*
run 125ns
