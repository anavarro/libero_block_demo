library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity block_tb is
end block_tb;

architecture behavioral of block_tb is

  constant CLK_PERIOD : time := 6.25 ns; -- 160MHZ

  signal clk : std_logic := '1';
  
  signal valid_arr : std_logic_vector(255 downto 0);
  signal index     : unsigned(7 downto 0);

begin


  block_top_inst: entity work.block_top
  port map(
    clk       => clk,
    valid_arr => valid_arr,
    index     => index
  );
  
  clk   <= not  clk after (  clk_period / 2.0 );

  process
  begin
    valid_arr <= (others => '0');

    wait for (CLK_PERIOD*5);
    
    valid_arr <= (7 => '1', others => '0');
    
    wait for CLK_PERIOD;
    
    valid_arr <= (7 downto 4 => '1', others => '0');
    
    wait for CLK_PERIOD;
    
    valid_arr <= (240 => '1', others => '0');
    
    wait for CLK_PERIOD;
    
    valid_arr <= (255 => '1', others => '0');
    
    wait for CLK_PERIOD;
    
    valid_arr <= (255 => '1', 7 downto 4 => '1', others => '0');
    
    wait;
  
  end process;

end behavioral;

